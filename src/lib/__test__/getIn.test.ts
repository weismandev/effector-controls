import { expect, test } from 'vitest';

import { getIn } from '@/src/lib';

const obj = {
  a: {
    b: 2,
    c: false,
    d: null,
  },
  t: true,
  s: 'a random string',
};

test('gets a value by array path', () => {
  expect(getIn(obj, ['a', 'b'])).toBe(2);
});

test('gets a value by string path', () => {
  expect(getIn(obj, 'a.b')).toBe(2);
});

test('return "undefined" if value was not found using given path', () => {
  expect(getIn(obj, 'a.z')).toBeUndefined();
});

test('return "undefined" if value was not found using given path and an intermediate value is "false"', () => {
  expect(getIn(obj, 'a.c.z')).toBeUndefined();
});

test('return "undefined" if value was not found using given path and an intermediate value is "null"', () => {
  expect(getIn(obj, 'a.d.z')).toBeUndefined();
});

test('return "undefined" if value was not found using given path and an intermediate value is "true"', () => {
  expect(getIn(obj, 't.z')).toBeUndefined();
});

test('return "undefined" if value was not found using given path and an intermediate value is a string', () => {
  expect(getIn(obj, 's.z')).toBeUndefined();
});
