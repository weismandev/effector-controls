export const buildUrl = (endpoint: string): string => {
  const apiUrl = localStorage.getItem('api') || 'https://api-product.mysmartflat.ru/api/';
  const token =
    localStorage.getItem('token') || 'ust-700695-0a0bde9af2b555766c664d08b28e5f62';

  return `${apiUrl}${endpoint}/?app=crm&token=${token}`;
};
