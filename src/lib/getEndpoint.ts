export const getEndpoint = () => {
  const localStorageEndpoint = localStorage.getItem('endpoint');

  return localStorageEndpoint
    ? localStorageEndpoint
    : 'https://api-product.mysmartflat.ru/api/';
};
