// import { Listbox } from '@features/effector-form/components/listbox';
// import {
//   ticketPrioritiesQuery,
//   TicketPriorityControlGate,
// } from '@features/effector-form/controls/ticketPriority/model';
// import {
//   Autocomplete,
//   FormControl,
//   FormHelperText,
//   FormLabel,
//   TextField,
// } from '@mui/material';
// import { useGate, useUnit } from 'effector-react';
//
// import { withControl } from '@/src/withControl';
//
// export const TicketPriorityControl = withControl(
//   ({ value, setValue, error, label, required, readOnly, disabled }) => {
//     useGate(TicketPriorityControlGate);
//
//     const { data, pending } = useUnit(ticketPrioritiesQuery);
//
//     return (
//       <FormControl required={required} disabled={disabled}>
//         <FormLabel>{label}</FormLabel>
//         <Autocomplete
//           loading={pending}
//           value={!data ? null : data?.find((item) => item.id === value)}
//           options={!data ? [] : data}
//           onChange={(event: any, newValue: any) => {
//             setValue(newValue?.id || null);
//           }}
//           ListboxComponent={Listbox}
//           renderInput={(params): JSX.Element => (
//             <TextField
//               {...params}
//               InputProps={{
//                 ...params.InputProps,
//                 readOnly,
//                 endAdornment: readOnly ? null : params.InputProps.endAdornment,
//               }}
//             />
//           )}
//           readOnly={readOnly}
//           disabled={disabled}
//         />
//         <FormHelperText error>{error}</FormHelperText>
//       </FormControl>
//     );
//   }
// );
