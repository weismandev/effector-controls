// import { Listbox } from '@features/effector-form/components/listbox';
// import { Autocomplete, FormControl, FormHelperText, FormLabel } from '@mui/material';
// import TextField from '@mui/material/TextField';
// import { useGate, useUnit } from 'effector-react';
//
// import { withControl } from '@/src/withControl';
//
// import { TicketOriginControlGate, ticketOriginsQuery } from './model';
//
// export const TicketOriginControl = withControl(
//   ({ value, setValue, error, label, required, readOnly, disabled }) => {
//     useGate(TicketOriginControlGate);
//
//     const { data, pending } = useUnit(ticketOriginsQuery);
//
//     return (
//       <FormControl required={required} disabled={disabled}>
//         <FormLabel>{label}</FormLabel>
//         <Autocomplete
//           loading={pending}
//           value={!data ? null : data?.find((item) => item.id === value)}
//           options={!data ? [] : data}
//           onChange={(event: any, newValue: any) => {
//             setValue(newValue?.id || null);
//           }}
//           ListboxComponent={Listbox}
//           renderInput={(params): JSX.Element => (
//             <TextField
//               {...params}
//               InputProps={{
//                 ...params.InputProps,
//                 readOnly,
//                 endAdornment: readOnly ? null : params.InputProps.endAdornment,
//               }}
//             />
//           )}
//           readOnly={readOnly}
//           disabled={disabled}
//         />
//         <FormHelperText error>{error}</FormHelperText>
//       </FormControl>
//     );
//   }
// );
