// import { Listbox } from '@features/effector-form/components/listbox';
// import {
//   TicketClassControlGate,
//   ticketClassesQuery,
// } from '@features/effector-form/controls/ticketClass/model';
// import { Autocomplete, FormControl, FormLabel, TextField } from '@mui/material';
// import { useGate, useUnit } from 'effector-react';
//
// import { withControl } from '@/src/withControl';
//
// export const TicketClassControl = withControl(
//   ({ value, setValue, error, label, required, readOnly, disabled }) => {
//     useGate(TicketClassControlGate);
//
//     const { data, pending } = useUnit(ticketClassesQuery);
//
//     return (
//       <FormControl required={required} disabled={disabled}>
//         <FormLabel>{label}</FormLabel>
//         <Autocomplete
//           loading={pending}
//           value={!data ? null : data?.find((item) => item.id === value)}
//           options={!data ? [] : data}
//           onChange={(event: any, newValue: any) => {
//             setValue(newValue?.id || null);
//           }}
//           ListboxComponent={Listbox}
//           renderInput={(params): JSX.Element => (
//             <TextField
//               {...params}
//               InputProps={{
//                 ...params.InputProps,
//                 readOnly,
//                 endAdornment: readOnly ? null : params.InputProps.endAdornment,
//               }}
//             />
//           )}
//           readOnly={readOnly}
//           disabled={disabled}
//         />
//       </FormControl>
//     );
//   }
// );
