// import { Listbox } from '@features/effector-form/components/listbox';
// import { FormControl, FormHelperText, FormLabel, TextField } from '@mui/material';
// import Autocomplete from '@mui/material/Autocomplete';
// import * as React from 'react';
//
// import { withControl } from '@/src/withControl';
//
// import { SelectControlType } from './types';
//
// export const SelectControl = withControl<SelectControlType>(
//   ({ value, setValue, error, label, required, readOnly, disabled, options }) => {
//     return (
//       <FormControl required={required} disabled={disabled}>
//         <FormLabel>{label}</FormLabel>
//         <Autocomplete
//           value={!options ? null : options?.find((item) => item.id === value)}
//           options={!options ? [] : options}
//           onChange={(event: any, newValue: any) => {
//             setValue(newValue.id);
//           }}
//           ListboxComponent={Listbox}
//           renderInput={(params): JSX.Element => (
//             <TextField
//               {...params}
//               InputProps={{
//                 ...params.InputProps,
//                 readOnly,
//                 endAdornment: readOnly ? null : params.InputProps.endAdornment,
//               }}
//             />
//           )}
//           readOnly={readOnly}
//           disabled={disabled}
//         />
//         <FormHelperText error>{error}</FormHelperText>
//       </FormControl>
//     );
//   }
// );
