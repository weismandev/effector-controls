import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import {
  createTestForm,
  LocalizationWrapper,
  submitRejected,
} from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();
  const control = render(
    <LocalizationWrapper>
      <Control.Time form={form} name="time" />{' '}
    </LocalizationWrapper>
  );

  return {
    form,
    user: userEvent.setup(),
    ...control,
    input: control.container.querySelector('input') as HTMLElement,
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(
    <LocalizationWrapper>
      <Control.Time form={form} name="time" />
    </LocalizationWrapper>
  );
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <LocalizationWrapper>
      <Control.Time form={form} name="time" label="Time" required disabled readOnly />
    </LocalizationWrapper>
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, '15:00');
  expect(form.$values.getState().time).toStrictEqual(new Date('2023-01-01 15:00:00'));
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({ path: 'time', value: new Date('2023-01-01 15:00:00') });
  });
  expect(screen.getByDisplayValue('15:00')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'time', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});
