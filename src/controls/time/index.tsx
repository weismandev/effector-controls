import { FormControl, FormHelperText, FormLabel } from '@mui/material';
import { TimeField, TimeFieldProps } from '@mui/x-date-pickers-pro';
import { useCallback } from 'react';

import { withControl } from '@/src/withControl';

export const TimeControl = withControl<TimeFieldProps<Date>>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = useCallback(
      (newValue: Date | null) => setValue(newValue),
      [value]
    );

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <TimeField
          {...props}
          value={value}
          onChange={handleChange}
          InputProps={{
            ...props.InputProps,
            error: isError,
            required,
            readOnly,
            disabled,
          }}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
