import {
  Autocomplete,
  AutocompleteProps,
  FormControl,
  FormHelperText,
  FormLabel,
} from '@mui/material';
import TextField from '@mui/material/TextField';
import { useGate, useUnit } from 'effector-react';

import { Listbox } from '@/src/components/listbox';
import { getAutocompleteValue } from '@/src/lib/getAutocompleteValue';
import { withControl } from '@/src/withControl';

import { BuildingControlGate, buildingQuery } from './model';
import { Building, BuildingControlProps } from './types';

export const BuildingControl = withControl<
  BuildingControlProps & Partial<AutocompleteProps<any, any, any, any>>
>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    multiple,
    complex,
    ...props
  }) => {
    useGate(BuildingControlGate, { complex });

    const { data: options, pending } = useUnit(buildingQuery);

    const handleChange = (event: any, newValue: any) => {
      if (multiple) {
        setValue(newValue.map((item: Building) => item.id) || null);
      } else {
        setValue(newValue?.id || null);
      }
    };

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <Autocomplete
          {...props}
          loading={pending}
          value={getAutocompleteValue(options, value, multiple)}
          options={!options ? [] : options}
          onChange={handleChange}
          ListboxComponent={Listbox}
          renderInput={(params) => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                error: isError,
                required,
                readOnly,
                disabled,
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
          multiple={multiple}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
