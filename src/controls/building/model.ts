import { createQuery } from '@farfetched/core';
import { createEffect, sample } from 'effector';
import { createGate } from 'effector-react';

import { buildingApi } from './api';
import { BuildingControlProps, BuildingRequest, BuildingResponse } from './types';

export const BuildingControlGate = createGate<BuildingControlProps>();

export const fxGetBuildings = createEffect<
  BuildingRequest,
  BuildingResponse,
  Error
>().use(buildingApi.getBuildings);

export const buildingQuery = createQuery({
  name: 'building',
  effect: fxGetBuildings,
  mapData: ({ result }) =>
    result.data.buildings.map((building) => ({
      id: building.building.id,
      label: building.building.title,
    })),
});

sample({
  clock: BuildingControlGate.open,
  fn: (data) => ({
    complex_id: data.complex,
  }),
  target: buildingQuery.start,
});
