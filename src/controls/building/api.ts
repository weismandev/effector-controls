import axios from 'axios';

import { buildUrl } from '@/src/lib/buildUrl';

import { BuildingRequest } from './types';

const getBuildings = (payload: BuildingRequest = {}) =>
  axios
    .get(buildUrl('v1/buildings/get-list-crm'), {
      params: { ...payload, per_page: 1000000 },
    })
    .then((res) => res.data);

export const buildingApi = { getBuildings };
