import { FormControl, FormHelperText, FormLabel } from '@mui/material';
import { DateTimeField, DateTimeFieldProps } from '@mui/x-date-pickers-pro';

import { withControl } from '@/src/withControl';

export const DateTimeControl = withControl<DateTimeFieldProps<Date>>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (newValue: Date | null) => setValue(newValue);

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <DateTimeField
          {...props}
          value={value}
          onChange={handleChange}
          InputProps={{
            ...props.InputProps,
            error: isError,
            required,
            readOnly,
            disabled,
          }}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
