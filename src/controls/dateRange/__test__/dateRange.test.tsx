import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import {
  createTestForm,
  LocalizationWrapper,
  submitRejected,
} from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();
  const control = render(
    <LocalizationWrapper>
      <Control.DateRange form={form} name="dateRange" />
    </LocalizationWrapper>
  );

  return {
    form,
    user: userEvent.setup(),
    ...control,
    input: control.container.querySelector('input') as HTMLElement,
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(
    <LocalizationWrapper>
      <Control.DateRange form={form} name="dateRange" />
    </LocalizationWrapper>
  );
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <LocalizationWrapper>
      <Control.DateRange
        form={form}
        name="dateRange"
        label="DateRange"
        required
        disabled
        readOnly
      />
    </LocalizationWrapper>
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, '02.02.202305.02.2023');
  expect(form.$values.getState().dateRange).toStrictEqual([
    new Date('2023-02-02'),
    new Date('2023-02-05'),
  ]);
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({
      path: 'dateRange',
      value: [new Date('2023-02-02'), new Date('2023-02-05')],
    });
  });
  expect(screen.getByDisplayValue('02.02.2023 – 05.02.2023')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'dateRange', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});
