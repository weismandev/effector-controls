import { FormControl, FormHelperText, FormLabel } from '@mui/material';
import {
  DateRange,
  SingleInputDateRangeField,
  SingleInputDateRangeFieldProps,
} from '@mui/x-date-pickers-pro';

import { withControl } from '@/src/withControl';

export const DateRangeControl = withControl<SingleInputDateRangeFieldProps<Date, any>>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (newValue: DateRange<Date>) => setValue(newValue);

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <SingleInputDateRangeField
          {...props}
          value={value}
          onChange={handleChange}
          InputProps={{
            ...props.InputProps,
            error: isError,
            required,
            readOnly,
            disabled,
          }}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
