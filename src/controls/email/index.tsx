import {
  FormControl,
  FormHelperText,
  FormLabel,
  OutlinedInput,
  OutlinedInputProps,
} from '@mui/material';
import { useIMask } from 'react-imask';

import { withControl } from '@/src/withControl';

export const EmailControl = withControl<OutlinedInputProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (newValue: string) => {
      if (newValue !== value) {
        setValue(newValue);
      }
    };

    const { ref } = useIMask(
      {
        mask: 'W@W.W',
        blocks: {
          W: {
            mask: /^\w+$/,
          },
        },
      },
      {
        onAccept: handleChange,
      }
    );

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <OutlinedInput
          {...props}
          value={value}
          error={isError}
          required={required}
          readOnly={readOnly}
          disabled={disabled}
          inputRef={ref}
          type="email"
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
