import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Email form={form} name="email" />),
    input: screen.getByDisplayValue('test@test.ru'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Email form={form} name="email" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Email form={form} name="email" label="Email" required disabled readOnly />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, 'test@test.ru');
  expect(form.$values.getState().email).toBe('test@test.ru');
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({ path: 'email', value: 'test@test.ru' });
  });
  expect(screen.getByDisplayValue('test@test.ru')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'email', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});
