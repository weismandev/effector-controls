import { Visibility, VisibilityOff } from '@mui/icons-material';
import {
  FormControl,
  FormHelperText,
  FormLabel,
  IconButton,
  InputAdornment,
  OutlinedInput,
  OutlinedInputProps,
} from '@mui/material';
import { ChangeEvent, MouseEvent, useState } from 'react';

import { withControl } from '@/src/withControl';

export const PasswordControl = withControl<OutlinedInputProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const [showPassword, setShowPassword] = useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: MouseEvent<HTMLButtonElement>) => {
      event.preventDefault();
    };

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value);

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <OutlinedInput
          {...props}
          value={value}
          error={isError}
          onChange={handleChange}
          required={required}
          readOnly={readOnly}
          disabled={disabled}
          type={showPassword ? 'text' : 'password'}
          autoComplete="current-password"
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
                size="small"
                disabled={disabled}
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
