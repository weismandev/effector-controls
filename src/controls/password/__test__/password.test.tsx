import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Password form={form} name="password" />),
    input: screen.getByDisplayValue('12345'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Password form={form} name="password" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Password
      form={form}
      name="password"
      label="Password"
      required
      disabled
      readOnly
    />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, '54321');
  expect(form.$values.getState().password).toBe('54321');
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({ path: 'password', value: '54321' });
  });
  expect(screen.getByDisplayValue('54321')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'password', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});

test('Переключение скрытого отображения ', async () => {
  const { user, container } = setup();
  await user.click(screen.getByRole('button'));
  expect(container.querySelector('input[type="text"]')).toBeInTheDocument();
});
