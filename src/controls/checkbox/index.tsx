import {
  Checkbox,
  CheckboxProps,
  FormControl,
  FormControlLabel,
  FormHelperText,
} from '@mui/material';
import { ChangeEvent } from 'react';

import { withControl } from '@/src/withControl';

export const CheckboxControl = withControl<CheckboxProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.checked);

    return (
      <FormControl>
        <FormControlLabel
          control={
            <Checkbox
              {...props}
              checked={value}
              onChange={handleChange}
              required={required}
              readOnly={readOnly}
              disabled={disabled}
            />
          }
          label={label}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
