import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Checkbox form={form} name="checkbox" />),
    input: screen.getByRole('checkbox'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Checkbox form={form} name="checkbox" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Checkbox
      form={form}
      name="checkbox"
      label="Checkbox"
      required
      disabled
      readOnly
    />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { user, input } = setup();
  await user.click(input);
  expect(input).not.toBeChecked();
});

test('Установка значения из формы', () => {
  const { form, input } = setup();
  act(() => {
    form.setValue({ path: 'checkbox', value: false });
  });
  expect(input).not.toBeChecked();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'checkbox', null);
  });

  expect(screen.getByText('Expected boolean, received null')).toBeInTheDocument();
});
