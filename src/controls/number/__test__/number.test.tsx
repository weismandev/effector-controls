import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Number form={form} name="number" />),
    input: screen.getByDisplayValue('10'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Number form={form} name="number" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Number form={form} name="number" label="Number" required disabled readOnly />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, '100');
  expect(form.$values.getState().number).toBe(100);
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({ path: 'number', value: 100 });
  });
  expect(screen.getByDisplayValue('100')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'number', 0);
  });

  expect(screen.getByText('Number must be greater than 0')).toBeInTheDocument();
});
