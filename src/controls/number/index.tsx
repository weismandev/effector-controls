import {
  FormControl,
  FormHelperText,
  FormLabel,
  OutlinedInput,
  OutlinedInputProps,
} from '@mui/material';
import { ChangeEvent, useMemo } from 'react';

import { withControl } from '@/src/withControl';

export const NumberControl = withControl<OutlinedInputProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) =>
      setValue(Number(e.target.value));

    // const { ref } = useIMask(
    //   { mask: /^\d+$/ },
    //   {
    //     onComplete: handleChange,
    //   }
    // );

    return useMemo(
      () => (
        <FormControl>
          {label && <FormLabel required={required}>{label}</FormLabel>}

          <OutlinedInput
            {...props}
            value={value}
            error={isError}
            onChange={handleChange}
            required={required}
            readOnly={readOnly}
            disabled={disabled}
            type="number"
          />

          {error && <FormHelperText error>{error}</FormHelperText>}
        </FormControl>
      ),
      [value, error]
    );
  }
);
