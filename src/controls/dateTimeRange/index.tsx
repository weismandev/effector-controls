import { FormControl, FormHelperText, FormLabel } from '@mui/material';
import {
  DateRange,
  SingleInputDateTimeRangeField,
  SingleInputDateTimeRangeFieldProps,
} from '@mui/x-date-pickers-pro';

import { withControl } from '@/src/withControl';

export const DateTimeRangeControl = withControl<SingleInputDateTimeRangeFieldProps<Date>>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (newValue: DateRange<Date>) => setValue(newValue);

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        {/*// @ts-ignore*/}
        <SingleInputDateTimeRangeField
          {...props}
          value={value}
          onChange={handleChange}
          InputProps={{
            ...props.InputProps,
            error: isError,
            required,
            readOnly,
            disabled,
          }}
        />
        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
