import axios from 'axios';

import { buildUrl } from '@/src/lib/buildUrl';

import { ApartmentRequest } from './types';

export const getApartments = (payload: ApartmentRequest = {}) =>
  axios
    .get(buildUrl('v1/apartment/simple-list'), {
      params: { ...payload, per_page: 1000000 },
    })
    .then((res) => res.data);

export const apartmentsApi = {
  getApartments,
};
