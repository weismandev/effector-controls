import '@testing-library/jest-dom';

import { act, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = async (props?: any) => {
  const form = createTestForm();
  const control = render(
    <Control.Apartment form={form} name="apartment" open complex={407} {...props} />
  );
  const options = await waitFor(() => screen.getAllByRole('option'), {
    timeout: 5000,
  });

  return {
    form,
    user: userEvent.setup(),
    ...control,
    input: control.container.querySelector('input') as HTMLElement,
    options,
  };
};

test('Базовый рендер', async () => {
  const { form } = await setup();
  const control = render(<Control.Apartment form={form} name="apartment" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', async () => {
  const { form } = await setup();
  const control = render(
    <Control.Apartment
      form={form}
      name="apartment"
      label="Apartment"
      required
      disabled
      readOnly
    />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, options } = await setup();
  await user.click(options[0]);

  expect(Boolean(form.$values.getState().apartment)).toBeTruthy();
});

test('Установка значения из формы', async () => {
  const { form } = await setup();

  act(() => {
    form.setValue({ path: 'apartment', value: 837322 });
  });

  expect(screen.getByDisplayValue('Сервисная квартира')).toBeInTheDocument();
});

test('Работа параметра complex', async () => {
  const { options: firstOptions } = await setup({ complex: 407 });
  const { options: secondOptions } = await setup({ complex: 26 });

  expect(firstOptions.length).not.toStrictEqual(secondOptions.length);
});

test('Работа параметра building', async () => {
  const { options: firstOptions } = await setup({ complex: 407, building: 4867 });
  const { options: secondOptions } = await setup({ complex: 26, building: 1807 });

  expect(firstOptions.length).not.toStrictEqual(secondOptions.length);
});

test('Отображение ошибки', async () => {
  const { form } = await setup();

  await act(async () => {
    await submitRejected(form, 'apartment', 0);
  });

  expect(
    screen.getByText('Number must be greater than or equal to 1')
  ).toBeInTheDocument();
});
