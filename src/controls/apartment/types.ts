export type Apartment = {
  id: number;
  title: string;
};

export type ApartmentRequest = {
  complex_id?: number;
  building_id?: number;
};

export type ApartmentResponse = {
  data: {
    items: {
      apartment: Apartment;
    }[];
  };
};

export type ApartmentGate = {
  complex?: number;
  building?: number;
};

export type ApartmentControlType = {
  complex?: number;
  building?: number;
};
