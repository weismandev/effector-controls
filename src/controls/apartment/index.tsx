import {
  AutocompleteProps,
  FormControl,
  FormHelperText,
  FormLabel,
  TextField,
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import { useGate, useUnit } from 'effector-react';
import * as React from 'react';

import { Listbox } from '@/src/components/listbox';
import { getAutocompleteValue } from '@/src/lib/getAutocompleteValue';
import { withControl } from '@/src/withControl';

import { ApartmentControlGate, apartmentsQuery } from './model';
import { Apartment, ApartmentControlType } from './types';

export const ApartmentControl = withControl<
  ApartmentControlType & Partial<AutocompleteProps<any, any, any, any>>
>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    multiple,
    complex,
    building,
    ...props
  }) => {
    useGate(ApartmentControlGate, { complex, building });

    const { data: options, pending } = useUnit(apartmentsQuery);

    const handleChange = (event: any, newValue: any) => {
      if (multiple) {
        setValue(newValue.map((item: Apartment) => item.id) || null);
      } else {
        setValue(newValue?.id || null);
      }
    };

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <Autocomplete
          {...props}
          loading={pending}
          value={getAutocompleteValue(options, value, multiple)}
          options={!options ? [] : options}
          onChange={handleChange}
          ListboxComponent={Listbox}
          renderInput={(params) => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                error: isError,
                required,
                readOnly,
                disabled,
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
          multiple={multiple}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
