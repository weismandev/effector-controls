import { createQuery } from '@farfetched/core';
import { createEffect, sample } from 'effector';
import { createGate } from 'effector-react';

import { apartmentsApi } from './api';
import { ApartmentGate, ApartmentRequest, ApartmentResponse } from './types';

export const ApartmentControlGate = createGate<ApartmentGate>();

const fxGetApartments = createEffect<ApartmentRequest, ApartmentResponse, Error>().use(
  apartmentsApi.getApartments
);

export const apartmentsQuery = createQuery({
  name: 'apartments',
  effect: fxGetApartments,
  mapData: ({ result, params }) =>
    result.data.items.map(({ apartment }) => ({
      id: apartment.id,
      label: apartment.title,
    })),
});

sample({
  clock: ApartmentControlGate.state,
  filter: (data) => Boolean(data),
  fn: (data) => ({
    complex_id: data.complex,
    building_id: data.building,
  }),
  target: apartmentsQuery.start,
});
