import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Phone form={form} name="phone" />),
    input: screen.getByDisplayValue('+79999999999'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Phone form={form} name="phone" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Phone form={form} name="phone" label="Phone" required disabled readOnly />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, '+78888888888');
  expect(form.$values.getState().phone).toBe('+78888888888');
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({ path: 'phone', value: '+78888888888' });
  });
  expect(screen.getByDisplayValue('+78888888888')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'phone', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});
