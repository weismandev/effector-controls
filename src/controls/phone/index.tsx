import {
  FormControl,
  FormHelperText,
  FormLabel,
  OutlinedInput,
  OutlinedInputProps,
} from '@mui/material';
import { useCallback } from 'react';
import { useIMask } from 'react-imask';

import { withControl } from '@/src/withControl';

export const PhoneControl = withControl<OutlinedInputProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleAccept = useCallback(
      (newValue: string) => {
        if (newValue !== value) {
          setValue(newValue);
        }
      },
      [value]
    );

    const handleComplete = useCallback(
      (newValue: string) => {
        setValue(newValue);
      },
      [value]
    );

    const { ref } = useIMask(
      { mask: '+{7}0000000000' },
      {
        onAccept: handleAccept,
        onComplete: handleComplete,
      }
    );

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <OutlinedInput
          {...props}
          value={value}
          error={isError}
          required={required}
          readOnly={readOnly}
          disabled={disabled}
          inputRef={ref}
          type="tel"
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
