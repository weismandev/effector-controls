import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Switch form={form} name="checkbox" />),
    input: screen.getByRole('checkbox'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Switch form={form} name="switch" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Switch form={form} name="switch" label="Switch" required disabled readOnly />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.click(input);
  expect(form.$values.getState().checkbox).toBe(false);
});

test('Установка значения из формы', () => {
  const { form, container } = setup();
  act(() => {
    form.setValue({ path: 'switch', value: false });
  });
  expect(container.querySelector('Mui-checked')).not.toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'checkbox', null);
  });

  expect(screen.getByText('Expected boolean, received null')).toBeInTheDocument();
});
