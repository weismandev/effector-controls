import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  Switch,
  SwitchProps,
} from '@mui/material';
import { ChangeEvent } from 'react';

import { withControl } from '@/src/withControl';

export const SwitchControl = withControl<SwitchProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.checked);

    return (
      <FormControl>
        <FormControlLabel
          control={
            <Switch
              {...props}
              checked={value}
              onChange={handleChange}
              required={required}
              readOnly={readOnly}
              disabled={disabled}
            />
          }
          label={label}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
