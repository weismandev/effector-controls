import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import {
  createTestForm,
  LocalizationWrapper,
  submitRejected,
} from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();
  const control = render(
    <LocalizationWrapper>
      <Control.TimeRange form={form} name="timeRange" />{' '}
    </LocalizationWrapper>
  );

  return {
    form,
    user: userEvent.setup(),
    ...control,
    input: control.container.querySelector('input') as HTMLElement,
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(
    <LocalizationWrapper>
      <Control.TimeRange form={form} name="timeRange" />
    </LocalizationWrapper>
  );
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <LocalizationWrapper>
      <Control.TimeRange
        form={form}
        name="timeRange"
        label="TimeRange"
        required
        disabled
        readOnly
      />
    </LocalizationWrapper>
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, '10:0015:00');
  expect(form.$values.getState().timeRange).toStrictEqual([
    new Date('2023-01-01 10:00:00'),
    new Date('2023-01-01 15:00:00'),
  ]);
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({
      path: 'timeRange',
      value: [new Date('2023-01-01 10:00:00'), new Date('2023-01-01 15:00:00')],
    });
  });
  expect(screen.getByDisplayValue('10:00 – 15:00')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'timeRange', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});
