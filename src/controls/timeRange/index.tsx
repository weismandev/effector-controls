import { FormControl, FormHelperText, FormLabel } from '@mui/material';
import {
  DateRange,
  SingleInputTimeRangeField,
  SingleInputTimeRangeFieldProps,
} from '@mui/x-date-pickers-pro';
import { useCallback } from 'react';

import { withControl } from '@/src/withControl';

export const TimeRangeControl = withControl<SingleInputTimeRangeFieldProps<Date>>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = useCallback(
      (newValue: DateRange<Date>) => setValue(newValue),
      [value]
    );

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        {/*// @ts-ignore*/}
        <SingleInputTimeRangeField
          {...props}
          value={value}
          onChange={handleChange}
          InputProps={{
            ...props.InputProps,
            error: isError,
            required,
            readOnly,
            disabled,
          }}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
