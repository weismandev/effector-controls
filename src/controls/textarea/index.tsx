import {
  FormControl,
  FormHelperText,
  FormLabel,
  OutlinedInput,
  OutlinedInputProps,
} from '@mui/material';
import { ChangeEvent, useMemo } from 'react';

import { withControl } from '@/src/withControl';

export const TextareaControl = withControl<OutlinedInputProps>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    ...props
  }) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.value);

    return useMemo(
      () => (
        <FormControl>
          {label && <FormLabel required={required}>{label}</FormLabel>}

          <OutlinedInput
            {...props}
            value={value}
            error={isError}
            onChange={handleChange}
            required={required}
            readOnly={readOnly}
            disabled={disabled}
            multiline
            minRows={3}
            maxRows={10}
          />

          {error && <FormHelperText error>{error}</FormHelperText>}
        </FormControl>
      ),
      [value, error]
    );
  }
);
