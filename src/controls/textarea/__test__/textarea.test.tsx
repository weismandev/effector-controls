import '@testing-library/jest-dom';

import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = () => {
  const form = createTestForm();

  return {
    form,
    user: userEvent.setup(),
    ...render(<Control.Textarea form={form} name="textarea" />),
    input: screen.getByDisplayValue('textarea'),
  };
};

test('Базовый рендер', () => {
  const { form } = setup();
  const control = render(<Control.Textarea form={form} name="textarea" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', () => {
  const { form } = setup();
  const control = render(
    <Control.Text
      form={form}
      name="textarea"
      label="Textarea"
      required
      disabled
      readOnly
    />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, input } = setup();
  await user.clear(input);
  await user.type(input, 'test');
  expect(form.$values.getState().textarea).toBe('test');
});

test('Установка значения из формы', () => {
  const { form } = setup();
  act(() => {
    form.setValue({ path: 'textarea', value: 'testForm' });
  });
  expect(screen.getByDisplayValue('testForm')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = setup();

  await act(async () => {
    await submitRejected(form, 'textarea', '');
  });

  expect(
    screen.getByText('String must contain at least 1 character(s)')
  ).toBeInTheDocument();
});
