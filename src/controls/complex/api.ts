import axios from 'axios';

import { buildUrl } from '@/src/lib/buildUrl';

const getComplexes = () => axios.get(buildUrl('v1/complex/list')).then((res) => res.data);

export const complexApi = {
  getComplexes,
};
