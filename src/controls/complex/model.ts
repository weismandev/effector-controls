import { createQuery } from '@farfetched/core';
import { createEffect, sample } from 'effector';
import { createGate } from 'effector-react';

import { complexApi } from './api';
import { ComplexResponse } from './types';

export const ComplexControlGate = createGate();

const fxGetComplexes = createEffect<void, ComplexResponse, Error>().use(
  complexApi.getComplexes
);

export const complexQuery = createQuery({
  name: 'complex',
  effect: fxGetComplexes,
  mapData: ({ result, params }) =>
    result.data.items.map((complex) => ({
      id: complex.id,
      label: complex.title,
    })),
});

// cache(complexQuery, {
//   adapter: localStorageCache({ maxAge: '5m' }),
// });

sample({
  clock: ComplexControlGate.open,
  target: complexQuery.start,
});
