import '@testing-library/jest-dom';

import { act, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { expect, test } from 'vitest';

import { Control } from '@/src';
import { createTestForm, submitRejected } from '@/src/__test__/utils';

const setup = async () => {
  const form = createTestForm();
  const control = render(<Control.Complex form={form} name="complex" open />);
  const options = await waitFor(() => screen.getAllByRole('option'), {
    timeout: 5000,
  });

  return {
    form,
    user: userEvent.setup(),
    ...control,
    input: control.container.querySelector('input') as HTMLElement,
    options,
  };
};

test('Базовый рендер', async () => {
  const { form } = await setup();
  const control = render(<Control.Complex form={form} name="complex" />);
  expect(control).toMatchSnapshot();
});

test('Рендер с минимальными параметрами', async () => {
  const { form } = await setup();
  const control = render(
    <Control.Complex
      form={form}
      name="complex"
      label="Complex"
      required
      disabled
      readOnly
    />
  );
  expect(control).toMatchSnapshot();
});

test('Установка значения', async () => {
  const { form, user, options } = await setup();
  await user.click(options[0]);

  expect(Boolean(form.$values.getState().complex)).toBeTruthy();
});

test('Установка значения из формы', async () => {
  const { form } = await setup();

  act(() => {
    form.setValue({ path: 'complex', value: 407 });
  });

  expect(screen.getByDisplayValue('ЖК Тестовый')).toBeInTheDocument();
});

test('Отображение ошибки', async () => {
  const { form } = await setup();

  await act(async () => {
    await submitRejected(form, 'complex', 0);
  });

  expect(
    screen.getByText('Number must be greater than or equal to 1')
  ).toBeInTheDocument();
});
