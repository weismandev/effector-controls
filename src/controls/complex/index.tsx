import {
  Autocomplete,
  AutocompleteProps,
  FormControl,
  FormHelperText,
  FormLabel,
} from '@mui/material';
import TextField from '@mui/material/TextField';
import { useGate, useUnit } from 'effector-react';

import { Listbox } from '@/src/components/listbox';
import { getAutocompleteValue } from '@/src/lib/getAutocompleteValue';
import { withControl } from '@/src/withControl';

import { ComplexControlGate, complexQuery } from './model';
import { Complex } from './types';

export const ComplexControl = withControl<Partial<AutocompleteProps<any, any, any, any>>>(
  ({
    label,
    value,
    setValue,
    error,
    isError,
    required,
    readOnly,
    disabled,
    multiple,
    ...props
  }) => {
    useGate(ComplexControlGate);

    const { data: options, pending } = useUnit(complexQuery);

    const handleChange = (event: any, newValue: any) => {
      if (multiple) {
        setValue(newValue.map((item: Complex) => item.id) || null);
      } else {
        setValue(newValue?.id || null);
      }
    };

    return (
      <FormControl>
        {label && <FormLabel required={required}>{label}</FormLabel>}

        <Autocomplete
          {...props}
          loading={pending}
          value={getAutocompleteValue(options, value, multiple)}
          options={!options ? [] : options}
          onChange={handleChange}
          ListboxComponent={Listbox}
          renderInput={(params) => (
            <TextField
              {...params}
              InputProps={{
                ...params.InputProps,
                error: isError,
                required,
                readOnly,
                disabled,
              }}
            />
          )}
          readOnly={readOnly}
          disabled={disabled}
          multiple={multiple}
        />

        {error && <FormHelperText error>{error}</FormHelperText>}
      </FormControl>
    );
  }
);
