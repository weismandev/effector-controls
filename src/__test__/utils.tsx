import { LocalizationProvider } from '@mui/x-date-pickers-pro';
import { AdapterDateFns } from '@mui/x-date-pickers-pro/AdapterDateFns';
import { createForm, FormInstance } from '@unicorn/effector-form';
import ru from 'date-fns/locale/ru';
import React, { FC } from 'react';
import * as Zod from 'zod';

export const createTestForm = () =>
  createForm({
    initialValues: {
      text: 'text',
      number: 10,
      textarea: 'textarea',
      checkbox: true,
      switch: true,

      email: 'test@test.ru',
      password: '12345',
      phone: '+79999999999',

      date: new Date('2023-01-01'),
      dateRange: [new Date('2023-01-01'), new Date('2023-02-02')],
      dateTime: new Date('2023-01-01 10:00'),
      dateTimeRange: [new Date('2023-01-01 10:00'), new Date('2023-02-02 10:00')],
      time: new Date('2023-01-01 10:00'),
      timeRange: [new Date('2023-01-01 10:00'), new Date('2023-01-01 15:00')],

      complex: null,
      building: null,
      apartment: null,
    },
    // @ts-ignore
    validationSchema: Zod.object({
      text: Zod.string().nonempty(),
      number: Zod.number().positive(),
      textarea: Zod.string().nonempty(),
      checkbox: Zod.boolean(),
      email: Zod.string().nonempty(),
      password: Zod.string().nonempty(),
      phone: Zod.string().nonempty(),
      date: Zod.string().nonempty(),
      dateRange: Zod.string().nonempty(),
      dateTime: Zod.string().nonempty(),
      dateTimeRange: Zod.string().nonempty(),
      time: Zod.string().nonempty(),
      timeRange: Zod.string().nonempty(),
      complex: Zod.number().min(1),
      building: Zod.number().min(1),
      apartment: Zod.number().min(1),
    }).required({
      checkbox: true,
    }),
  });

export const submitAccepted = (form: FormInstance<any>, path: string, value: any) =>
  new Promise((resolve) => {
    form.$values.watch((values: any) => {
      resolve(values);
    });

    form.setValue({ path, value });

    form.submit();
  });

export const submitRejected = (form: FormInstance<any>, path: string, value: any) =>
  new Promise((resolve) => {
    form.rejected.watch((errors: any) => {
      resolve(errors);
    });

    form.setValue({ path, value });

    form.submit();
  });

export const LocalizationWrapper: FC<any> = ({ children }) => (
  <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={ru}>
    {children}
  </LocalizationProvider>
);
