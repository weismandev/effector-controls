import { ApartmentControl } from '@/src/controls/apartment';
import { BuildingControl } from '@/src/controls/building';
import { CheckboxControl } from '@/src/controls/checkbox';
import { ComplexControl } from '@/src/controls/complex';
import { DateControl } from '@/src/controls/date';
import { DateRangeControl } from '@/src/controls/dateRange';
import { DateTimeControl } from '@/src/controls/dateTime';
import { DateTimeRangeControl } from '@/src/controls/dateTimeRange';
import { EmailControl } from '@/src/controls/email';
import { NumberControl } from '@/src/controls/number';
import { PasswordControl } from '@/src/controls/password';
import { PhoneControl } from '@/src/controls/phone';
import { SwitchControl } from '@/src/controls/switch';
import { TextControl } from '@/src/controls/text';
import { TextareaControl } from '@/src/controls/textarea';
import { TimeControl } from '@/src/controls/time';
import { TimeRangeControl } from '@/src/controls/timeRange';

export const Control = {
  Text: TextControl,
  Number: NumberControl,
  Textarea: TextareaControl,
  Checkbox: CheckboxControl,
  Switch: SwitchControl,

  Email: EmailControl,
  Password: PasswordControl,
  Phone: PhoneControl,

  Date: DateControl,
  DateRange: DateRangeControl,
  DateTime: DateTimeControl,
  DateTimeRange: DateTimeRangeControl,
  Time: TimeControl,
  TimeRange: TimeRangeControl,

  Complex: ComplexControl,
  Building: BuildingControl,
  Apartment: ApartmentControl,

  // TicketClass: TicketClassControl,
  // TicketStatus: TicketStatusControl,
  // TicketType: TicketTypeControl,
  // TicketPriority: TicketPriorityControl,
  // TicketOrigin: TicketOriginControl,

  // Select: SelectControl,
};
