import { styled } from '@mui/material/styles';

export const Container = styled('div')(() => ({
  display: 'grid',
  gap: 16,
  width: '50%',
}));
