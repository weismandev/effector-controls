import { createForm } from '@unicorn/effector-form';
import * as Zod from 'zod';

export const controlsForm = createForm({
  initialValues: {
    text: 'text',
    number: 10,
    textarea: 'textarea',
    checkbox: true,
    switch: true,

    email: 'test@test.ru',
    password: '12345',
    phone: '+79999999999',

    date: new Date('2023-01-01'),
    dateRange: [new Date('2023-01-01'), new Date('2023-02-02')],
    dateTime: new Date('2023-01-01 10:00'),
    dateTimeRange: [new Date('2023-01-01 10:00'), new Date('2023-02-02 10:00')],
    time: new Date('2023-01-01 10:00'),
    timeRange: [new Date('2023-01-01 10:00'), new Date('2023-01-01 15:00')],

    complex: 407,
    buildings: 872,
    apartments: [],
  },
  validationSchema: Zod.object({
    text: Zod.string().nonempty(),
  }),
  debug: true,
});
