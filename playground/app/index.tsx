import { Button } from '@mui/material';
import { useUnit } from 'effector-react';

import { controlsForm } from '@/playground/app/model';
import { Control } from '@/src';

import * as Styled from './styled';

export const App = () => {
  const { values } = useUnit(controlsForm);

  const renderMany = () =>
    Array(1000)
      .fill(1)
      .map((item) => <Control.Text form={controlsForm} name="textd" label="Text" />);

  return (
    <Styled.Container>
      <Button onClick={() => controlsForm.setValue({ path: 'complex', value: 407 })}>
        press
      </Button>

      <Control.Complex form={controlsForm} name="complex" />
      <Control.Building form={controlsForm} name="building" complex={values.complex} />

      <Control.Apartment
        form={controlsForm}
        name="apartment"
        label="Apartment"
        complex={407}
        building={4867}
        open
      />

      {/*<Control.Number form={controlsForm} name="number" label="Number" required />*/}
      {/*<Control.Textarea form={controlsForm} name="textarea" label="Textarea" required />*/}
      {/*<Control.Checkbox form={controlsForm} name="checkbox" label="Checkbox" required />*/}
      {/*<Control.Switch form={controlsForm} name="switch" label="Switch" required />*/}

      {/*<Control.Email form={controlsForm} name="email" label="Email" required />*/}
      {/*<Control.Password form={controlsForm} name="password" label="Password" required />*/}
      {/*<Control.Phone form={controlsForm} name="phone" label="Phone" />*/}

      {/*<Control.Date form={controlsForm} name="date" label="Date" required />*/}
      {/*<Control.DateRange*/}
      {/*  form={controlsForm}*/}
      {/*  name="dateRange"*/}
      {/*  label="DateRange"*/}
      {/*  required*/}
      {/*/>*/}
      {/*<Control.DateTime form={controlsForm} name="dateTime" label="DateTime" required />*/}
      {/*<Control.DateTimeRange*/}
      {/*  form={controlsForm}*/}
      {/*  name="dateTimeRange"*/}
      {/*  label="DateTimeRange"*/}
      {/*  required*/}
      {/*/>*/}
      {/*<Control.Time form={controlsForm} name="time" label="Time" required />*/}
      {/*<Control.TimeRange*/}
      {/*  form={controlsForm}*/}
      {/*  name="timeRange"*/}
      {/*  label="TimeRange"*/}
      {/*  required*/}
      {/*/>*/}
    </Styled.Container>
  );
};
