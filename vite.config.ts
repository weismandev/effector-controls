import react from "@vitejs/plugin-react";
import path from "path";
import dts from "vite-plugin-dts";
import { defineConfig } from "vitest/config";
import svgr from "vite-plugin-svgr";

export default defineConfig({
  base: "./",
  plugins: [
    react({
      babel: {
        plugins: ["effector/babel-plugin"],
      },
    }),
    dts(),
    svgr({
      include: "**/*.svg?react",
    }),
  ],
  test: {
    include: ["src/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}"],
    globals: true,
    environment: "jsdom",
  },
  resolve: {
    alias: {
      "@": "",
    },
    dedupe: ["react", "react-dom", "react/jsx-runtime"],
  },
  server: {
    port: 3000,
    hmr: true,
  },
  build: {
    outDir: "dist",
    rollupOptions: {
      external: ["react", "react-dom", "react-router-dom"],
      output: {
        globals: {
          react: "React",
          "react-dom": "ReactDOM",
          "react-router-dom": "ReactRouterDOM",
          // 'react/jsx-runtime': 'jsxRuntime',
        },
      },
    },
    // emptyOutDir: true,
    // sourcemap: true,
    lib: {
      entry: path.resolve(__dirname, "src/index.ts"),
      name: "eslintConfig",
      formats: ["es", "cjs", "umd"],
      fileName: "index",
    },
  },
});
