# withControl

Компонент высшего порядка для удобного создания контролов.
Встроенные пропсы (name и label) можно расширить, достаточно при создании контрола указать тип пропсов в дженерике

```tsx
type MyControlProps = {
  customProp?: number
}

const MyControl = withControl<MyControlProps>(
  ({ value, customProp }) => {
    ... рендер контрола
  }
);
```

Использование кастомных пропсов не отличается от встроенных

```tsx
const { values } = useUnit(myForm);

<Form form={myForm}>
  <MyControl name="myControl" label="My control" customProp={10} />
</Form>;
```

Для более сложных вычислений кастомных пропсов рекомендуется перенести логику в модель и использовать в качестве стора

`model`

```ts
// Создаем стор с кастомными параметрами
export const $myControlParams = createStore<{ customProp: number }>(null);

sample({
  // Подписываемся на изменение значений формы
  clock: myForm.setValue,
  // Фильтруем только по изменению контрола с именем 'anotherControl'
  filter: ({ path, value }) => path === 'anotherControl' && value,
  // Мутируем перехваченное значение
  fn: (objectId, { value }) => ({ customProp: `20-${value}` }),
  // Отправляем в стор с кастомными параметрами
  target: $buildingParams,
});
```

`view`

```tsx
const [myControlParams] = useUnit([$myControlParams]);

<Form form={myForm}>
  <MyControl name="myControl" label="My control" {...myControlParams} />
</Form>;
```
